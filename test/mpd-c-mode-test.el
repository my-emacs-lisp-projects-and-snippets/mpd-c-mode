;;; mpd-c-mode-test.el --- tests for mpd-c-mode.el

;; Author: Erik Lundstedt
;; Maintainer: Erik Lundstedt
;; Version: 0.1
;; Homepage: https://gitlab.com/erik.lundstedt/mpd-c-mode
;; Package-Requires: ((emacs "25.1")))


;; This file is not part of GNU Emacs

;; This software is free and open source under the wonderful Don't be a jerk License
;; Enjoy your free software!

;;; Code:

(require 'ert)
(require 'mpd-c-mode)

(ert-deftest mpd-c-mode-test/sanity-test ()
  (should (equal 1 1)))

(provide 'mpd-c-mode-test)

;;; mpd-c-mode-test.el ends here
